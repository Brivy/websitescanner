# Import requests (to download the page)
import requests
# Import BeautifulSoup (to parse what we download)
from bs4 import BeautifulSoup
# Import Time (to add a delay between the times the scape runs)
import time
# Import smtplib (to allow us to email)
import smtplib
# Log the given info to a file
import logging
# Time is for logging
import datetime

################
##EMAIL FUNCTION##
###############
'''
Stappen:
 - GMAIL variablen. Alleen veranderen als er niet met google gewerkt word.
'''


def send_email(user, pwd, recipient, subject, body, currentTime):
    FROM = user
    TO = recipient if isinstance(recipient, list) else [recipient]
    SUBJECT = subject
    TEXT = body

    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(user, pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        logging.info("[" + currentTime + "] " + "Email verstuurd!")
    except:
        logging.error("[" + currentTime + "] " + "Fout: geen email verstuurd")


###################
##CHECK SITE NAAR ITEM##
###################
'''
Stappen:
 - Vervang de lege variabelen door jouw gegevens
'''


def checkSite(url, item, currentTime):
    try :
        headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        response = requests.get(url, headers=headers)
        soup = BeautifulSoup(response.text, "html.parser")

        if str(soup).find(item) == -1:
            place = url.split("/")
            logging.info("[" + currentTime + "] " + place[2] + " is nog steeds uitverkocht")
        else:

            verstuurder = ""
            wachtwoord = ""
            ontvanger = ""
            onderwerp = ""
            inhoud = url
            huidige_tijd = currentTime

            logging.info("[" + currentTime + "] " + "JACKPOT")
            send_email(verstuurder, wachtwoord, ontvanger, onderwerp, inhoud, huidige_tijd)
            return
    except TimeoutError:
        logging.info(url + " heeft een fout")
        return


###############
##MAIN FUNCTION##
###############
'''
 - urlDict: Het eerste woord van de urlDict bevat de website pagina. Het tweede woord zal het woord moeten zijn wat je wil zoeken.
 - loggintime: bepaal om de hoeveel seconden een website gecontroleerd moeten worden.
'''

urlDict = [["Website", "Woord dat gevonden moet worden"]]

logging.basicConfig(filename="logStores.log", level=logging.INFO, filemode="w")
loggintime = 60 / len(urlDict) * 2
logging.info("Tijd tussen zoekberichten: " + str(loggintime) + " seconden")

while True:
    for site in urlDict:
        currentTime = str(datetime.datetime.now()).split('.')
        checkSite(site[0], site[1], currentTime[0])
        time.sleep(loggintime)
